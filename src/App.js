import React, { Component } from 'react';
import {connect} from "react-redux";

import './App.css';

import {changeUrlHandler, shortenUrl} from "./store/actions";

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1 className="Title">Shorten your link!</h1>
        <input
          type="url"
          value={this.props.originalUrl}
          onChange={(e) => this.props.onChangeHandler(e)}
        />
        <button
          className="Btn"
          onClick={() => this.props.onSubmitHandler()}
        >Shorten</button>
        <h3 className="Subtitle">Your link now looks like this:</h3>
        <a href={this.props.shortUrl}>{this.props.shortUrl}</a>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    shortUrl: state.shortUrl,
    originalUrl: state.originalUrl
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeHandler: e => dispatch(changeUrlHandler(e)),
    onSubmitHandler: () => dispatch(shortenUrl())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);