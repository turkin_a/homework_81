import * as actionTypes from './actionTypes';
import axios from '../axios-api';
import config from "../config";

export const changeUrlHandler = event => {
  return {type: actionTypes.CHANGE_URL_HANDLER, value: event.target.value};
};

export const shortenUrlSuccess = shortUrl => {
  return {type: actionTypes.SHORTEN_URL_SUCCESS, shortUrl};
};

export const shortenUrl = () => {
  return (dispatch, getState) => {
    return axios.post('/links', {url: getState().originalUrl})
      .then(response => dispatch(shortenUrlSuccess(config.apiUrl + response.data.shortUrl)))
      .catch(error => console.log(error));
  }
};