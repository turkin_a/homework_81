import * as actionTypes from './actionTypes';

const initialState = {
  shortUrl: '',
  originalUrl: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_URL_HANDLER:
      return {...state, originalUrl: action.value, shortUrl: ''};
    case actionTypes.SHORTEN_URL_SUCCESS:
      return {...state, shortUrl: action.shortUrl};
    default:
      return state;
  }
};

export default reducer;